# =============================================================================
# File  : CPlayer.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 05
# =============================================================================
# This is the implementation of a player. Currently, this player class is very
# dry and bare, but is planned for an overhaul to handle saving and loading a
# player's game statistics.
# =============================================================================


class CPlayer:
    # === __init__ ============================================================
    # This is the class initializer method.
    #
    #    input:
    #            name    --  player name
    #            tokens  --  total number of starting tokens
    #            wins    --  total number of game wins
    #
    #    output:
    #            nothing
    # =========================================================================

    def __init__(self, name = "No Name", tokens = 0, wins = 0):
        self.name = name
        self.totalTokens = tokens
        self.totalWins = wins
        # this will be a surface and rectangle tuple: (surface, rectangle)
        self.avatar = None
        self.tokenPositions = []
# =============================================================================
# File  : CBoard.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 05
# =============================================================================
# This class simulates a checkerboard used for playing games.
# =============================================================================

import pygame


class CBoard():
    # === __init__ ============================================================
    # This is the class initializer method.
    #
    #    input:
    #            squareWidth     --  width of each square on the board
    #            squareHeight    --  height of each square on the board
    #            boardColor      --  color used for the board
    #            squareColor     --  color used for the playable board squares
    #
    #    output:
    #            nothing
    # =========================================================================

    def __init__(self, squareWidth = 64, squareHeight = 60, boardColor = "#FF0000", squareColor = "#000000"):
        self.board = pygame.Surface((squareWidth * 8, squareHeight * 8))
        self.squareRect = pygame.Rect(0, 0, squareWidth, squareHeight)
        self.squareWidth = squareWidth
        self.squareHeight = squareHeight
        self.squarePositions = []
        self.boardColor = pygame.color.Color(boardColor)
        self.squareColor = pygame.color.Color(squareColor)


    # === Initialize ==========================================================
    # This method fully sets up a checkerboard to be used
    #
    #    input:
    #            nothing
    #
    #    output:
    #            checkerboard is initialized and ready for playing
    # =========================================================================

    def Initialize(self):
        # draw empty checker board, which also creates a list of all playable
        # square positions
        self.Draw()


    # === Draw ================================================================
    # This method draws the checkerboard to a surface and saves the (x,y) of
    # each playable square as a tuple in a self contained class list
    #
    #    input:
    #            nothing
    #
    #    output:
    #            squarePositions holds the coordinates of all playable squares
    # =========================================================================

    def Draw(self):
        self.squarePositions = []
        nextSquare = self.squareRect.copy()
        isSquarePlayable = True

        self.board.fill(self.boardColor)

        # draw rows of the board
        for _row in xrange(0, 8): 
            # draw the columns of the board
            for _column in xrange(0, 8):
                if (isSquarePlayable == True):
                    self.squarePositions.append( (nextSquare.left, nextSquare.top) )
                    self.board.fill(self.squareColor, nextSquare)

                nextSquare.left += self.squareWidth

                isSquarePlayable = not isSquarePlayable

            nextSquare.left = 0

            nextSquare.top += self.squareHeight

            isSquarePlayable = not isSquarePlayable
            
        self.board.convert()
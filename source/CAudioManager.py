# =============================================================================
# File  : CAudioManager.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 05
# =============================================================================
# This class handles loading audio files into memory.
# =============================================================================

import os
import pygame


class CAudioManager:
    # === init ================================================================
    # This method intializes the game object so that it may be manipulated.
    # 
    # input:
    #        self -- reference to the object
    #
    # output
    #        nothing
    # =========================================================================

    def __init__(self, directory):
        self.directory = directory

    def LoadSound(self, name):
        class NoneSound:
            def play(self):
                pass

        if not pygame.mixer:
            return NoneSound()

        fullname = os.path.join(self.directory, name)

        try:
            sound = pygame.mixer.Sound(fullname)
        except pygame.error, message:
            print 'Cannot load sound:', fullname
            raise SystemExit, message

        return sound
# =============================================================================
# File  : CToken.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 05
# =============================================================================
# This is the implementation of a player token
# =============================================================================

import pygame


class CToken(pygame.sprite.Sprite):
    # === __init__ ============================================================
    # This is the class initializer method.
    #
    #    input:
    #            image        --  spritesheet image
    #            rect         --  rect currently used for the spritesheet
    #            xPosition    --  token's x-coordinate
    #            yPosition    --  token's y-coordinate
    #            imageResize  --  size of subsurface (avatar) that the player
    #                             will see on the screen
    #
    #    output:
    #            nothing
    # =========================================================================
    
    def __init__(self, image, rect, xPosition = 0, yPosition = 0, imageResize = None):
        pygame.sprite.Sprite.__init__(self)
        
        # maintain the original sprite sheet so that we may switch our avatar
        # at a later time (i.e. when a token becomes a king)
        self.sheet = image
        self.sheetRect = rect
        self.imageResize = imageResize

        self.ChangeAvatar(self.sheetRect.y)

        self.rect = self.image.get_rect()
        self.rect.x = xPosition
        self.rect.y = yPosition


    # === update ==============================================================
    # This method (at the moment) tests if the token must become a king token.
    # I have this method to integrate with a sprite group's update mechanism
    #
    #    input:
    #            kingY      --  y-coordinate that will activate king status
    #            imageY     --  y-coordinate of king avatar on spritesheet
    #            kingGroup  --  group that holds current player kings
    #
    #    output:
    #            nothing
    # =========================================================================

    def update(self, kingY, imageY, kingGroup):
        self.KingMe(kingY, imageY, kingGroup)


    # === KingMe ==============================================================
    # This method will king a token, so long as it is not already one.
    #
    #    input:
    #            activateY  --  y-coordinate that will activate king status
    #            avatarY    --  y-coordinate of king avatar on spritesheet
    #            kings      --  group that holds current player kings
    #
    #    output:
    #            tokens that become kings will be appended to the kings list
    # =========================================================================

    def KingMe(self, activateY, avatarY, kings):
        if ( (self.rect.y == activateY)  &  (len(self.groups())) == 1):
            self.ChangeAvatar(avatarY)
            self.add(kings)


    # === ChangeAvatar ========================================================
    # This method will display a new avatar on a subsurface from its existing
    # spritesheet, replacing the previous subsurface.
    #
    #    input:
    #            sheetY  --  y-coordinate of avatar on spritesheet
    #
    #    output:
    #            token is represented by a new avatar on the screen
    # =========================================================================

    def ChangeAvatar(self, sheetY):
        self.sheetRect.y = sheetY
                
        self.image = self.sheet.subsurface(self.sheetRect)

        if (self.imageResize is not None):
            self.image = pygame.transform.scale(self.image, self.imageResize)    
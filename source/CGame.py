# =============================================================================
# File  : CGame.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 05
# =============================================================================
# This class is the main game loop for the game Checkers: on the battlefield.
# =============================================================================

import pygame, os, sys, fnmatch
from math import ceil
from pygame.constants import RLEACCEL, QUIT, KEYDOWN, K_UP, K_DOWN, K_RETURN, K_1, K_LEFT, K_RIGHT, K_SPACE, K_2, K_a, K_d 
from CAudioManager import CAudioManager
from CGraphicsManager import CGraphicsManager
from CPlayer import CPlayer
from CBoard import CBoard
from CToken import CToken

# center the graphics window
os.environ["SDL_VIDEO_CENTERED"] = "1"

# graphic and audio databases
graphicDB = []
audioDB = []

# indices of outer tuples of the graphicDB
SURFACE = 0
RECTANGLE = 1
G_KEY = 2

# indices of outer tuples of the audioDB
SOUND = 0
A_KEY = 1

# indices of player list
PLAYER1 = 0
PLAYER2 = 1

class CGame:
    # === init ================================================================
    # This is the class initializer method.
    # 
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================

    def __init__(self):
        # game states
        self.gameState = ["Controllers"]
        self.finished = False
        
        # video display size
        self.screenWidth, self.screenHeight = 640, 480
        self.screenSize = self.screenWidth, self.screenHeight

        # graphic and audio managers
        self.graphicsManager = CGraphicsManager("../graphics")
        self.audioManager = CAudioManager("../audio/effects")

        # players
        self.players = [CPlayer("Player 1", 12), CPlayer("Player 2", 12)]

        # counters
        self.FPS = 60
        self.drawCounter = 0


    # === Initialize ==========================================================
    # This method initializes the game object so that it may be run and
    # safely and correctly.
    # 
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================
    
    def Initialize(self):
        
        # put the mixer in optimal state
        pygame.mixer.pre_init(22050, -16, 2, 1024)
        pygame.mixer.init()
        
        # initialize all pygame modules
        pygame.init()
        
        # initialize clock for keeping track of frames per second
        self.clock = pygame.time.Clock()

        # initialize video display and double buffering
        #self.screen = pygame.display.set_mode(self.screenSize, pygame.HWSURFACE | pygame.DOUBLEBUF)
        self.screen = pygame.display.set_mode(self.screenSize, pygame.SWSURFACE)
        self.background = pygame.Surface(self.screen.get_size())
        pygame.display.set_caption("Checkers: on the battlefield (%d FPS)" % self.clock.get_fps())

        # initialize graphic and audio databases
        self.__InitializeGraphicsDatabase()
        self.__InitializeAudioDatabase()
        
        # game board
        self.board = CBoard()


    # === __InitializeGraphicsDatabase ========================================
    # This method loads all graphics into memory that are to be used throughout
    # the game.
    # 
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================

    def __InitializeGraphicsDatabase(self):
        print "Loading all in-game graphics...",

        for nextFile in os.listdir(self.graphicsManager.directory):
            if fnmatch.fnmatch(nextFile, "*.png"):
                # retrieve image tuple: (surface, rectangle)
                tempTuple = self.graphicsManager.LoadImage(nextFile, -1)
                tempList = list(tempTuple)
                # add key identifier to image tuple
                tempList.append(nextFile[: - 4]) # 4 == length of file extension
                # tuple: (surface, rectangle, key)
                graphicDB.append(tuple(tempList))
 
        print "Success"


    # === __InitializeAudioDatabase ===========================================
    # This method loads all sound effects into memory that are to be used
    # throughout the game.
    # 
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================

    def __InitializeAudioDatabase(self):
        print "Loading all in-game audio...",

        for nextFile in os.listdir(self.audioManager.directory):
            if fnmatch.fnmatch(nextFile, "*.ogg"):
                # retrieve sound
                temp = self.audioManager.LoadSound(nextFile)
                # add key identifier to sound, which is the name of the file
                # without the extension
                tempList = [temp, nextFile[: - 4]]
                # tuple: (sound, key)
                audioDB.append(tuple(tempList))

        print "Success"


    # === Run =================================================================
    # This method holds the states of the game and pops each state off a stack.
    # The stack is popped upon each re-entry to this method. If there are no
    # more game states on the stack, the game is over.
    # 
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================

    def Run(self):
        while (True):
            
            if (not self.gameState):
                break
            
            nextState = self.gameState.pop()
            
            print "Returned to main loop. Now switching to: " + nextState
            
            functionName = nextState.replace(" " , "")
            
            if hasattr(self, functionName):
                function = getattr(self, functionName)
                function()
            else:
                break

        print "Game over. Thanks for playing!"


    # === Controllers =========================================================
    # This is the controller state that enables both players to understand
    # how to use one keyboard to play the game by displaying instructions to
    # the screen.
    # 
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================

    def Controllers(self):
        controllers = next((listIndex for listIndex, listTuple in enumerate(graphicDB) if listTuple[G_KEY] == "controllers"), None)

        self.screen.blit(self.background, (0, 0))
        self.screen.blit(graphicDB[controllers][SURFACE], (0, 0))
        
        pygame.display.update() 

        while (not self.finished):
            self.clock.tick(self.FPS)
                
            for event in pygame.event.get():
                if (event.type == QUIT):
                    pygame.quit()
                    sys.exit()

                elif (event.type == KEYDOWN):
                    self.gameState.append("Title Screen")
                    self.finished = True

            self.screen.blit(graphicDB[controllers][SURFACE], (0, 0))
                
            pygame.display.set_caption("Checkers: on the battlefield (%d FPS)" % self.clock.get_fps())


    # === TitleScreen =========================================================
    # This method handles the title screen game state. Currently, this screen
    # only allows the user to start the game, but an options menu will be added
    # that allows the changing of the board color, music volume, etc.
    # 
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================

    def TitleScreen(self):
        self.finished = not self.finished
        
        # save the graphicDB and audioDB indices used in this state for optimal
        # performance throughout this state
        effect = next((listIndex for listIndex, listTuple in enumerate(audioDB) if listTuple[A_KEY] == "select"), None)
        title = next((listIndex for listIndex, listTuple in enumerate(graphicDB) if listTuple[G_KEY] == "title"), None)
        start = next((listIndex for listIndex, listTuple in enumerate(graphicDB) if listTuple[G_KEY] == "start"), None)
        options = next((listIndex for listIndex, listTuple in enumerate(graphicDB) if listTuple[G_KEY] == "options"), None)

        # coordinates that tell us where to begin drawing images to the screen
        animatePlayerX = 271
        animatePlayerY = 296
        offsetY = 27

        # make sure we adjust the width of rects to display a single image
        graphicDB[start][RECTANGLE].width /= 2
        graphicDB[options][RECTANGLE].width /= 2

        # show start button on menu as default selection
        graphicDB[start][RECTANGLE].left = 99

        pygame.mixer.music.load("../audio/music/title.ogg")

        self.screen.blit(self.background, (0, 0))
        self.screen.blit(graphicDB[title][SURFACE], graphicDB[title][RECTANGLE])
        pygame.display.update()
        
        while (not self.finished):
            self.clock.tick(self.FPS)
                
            for event in pygame.event.get():
                if (event.type == QUIT):
                    pygame.quit()
                    sys.exit()
        
                ### TitleScreenEventHandler
                if (event.type == KEYDOWN):
                    if (event.key == K_RETURN):
                        # this trigger is caused only when the user selects
                        # start from the title screen menu
                        if (graphicDB[start][RECTANGLE].left == 99):
                            length = int( ceil(audioDB[effect][SOUND].get_length()) ) * 1000
                            audioDB[effect][SOUND].play()
                            self.gameState.append("Player Selection")
                            self.finished = True
                    elif (event.key == K_UP):
                        graphicDB[start][RECTANGLE].left = 99
                        graphicDB[options][RECTANGLE].left = 0
                    elif (event.key == K_DOWN):                
                        graphicDB[start][RECTANGLE].left = 0
                        graphicDB[options][RECTANGLE].left = 128

            ### TitleScreenDraw
            self.screen.blit(graphicDB[start][SURFACE], (animatePlayerX, animatePlayerY), graphicDB[start][RECTANGLE])
            self.screen.blit(graphicDB[options][SURFACE], (animatePlayerX, animatePlayerY + offsetY), graphicDB[options][RECTANGLE])
 
            ### TitleScreenUpdate
            # restart the title screen music when it stops
            if (pygame.mixer.music.get_busy() == False):
                pygame.mixer.music.play()

            pygame.display.set_caption("Checkers: on the battlefield (%d FPS)" % self.clock.get_fps())
            
            # just update the menu since that's all that is changing on the screen
            pygame.display.update( [(animatePlayerX, animatePlayerY, graphicDB[start][RECTANGLE].width, graphicDB[start][RECTANGLE].height),
                                    (animatePlayerX, animatePlayerY + offsetY, graphicDB[options][RECTANGLE].width, graphicDB[options][RECTANGLE].height)] )

        # pause the game state until the last sound effect has finished playing
        pygame.time.wait(length)


    # === PlayerSelection =====================================================
    # This method handles the player selection game state.
    # 
    # input:
    #        self -- reference to the object
    #
    # output
    #        Player1 and player 2 have visual avatars as their game tokens.
    # =========================================================================

    def PlayerSelection(self):
        ANIMATE = pygame.USEREVENT

        self.finished = not self.finished        

        done = True
        player1 = not done
        player2 = not done

        greyBorderBG = next((listIndex for listIndex, listTuple in enumerate(graphicDB) if listTuple[G_KEY] == "greyBorderBG"), None)
        player1Choose = next((listIndex for listIndex, listTuple in enumerate(graphicDB) if listTuple[G_KEY] == "player1Choose"), None)
        player2Choose = next((listIndex for listIndex, listTuple in enumerate(graphicDB) if listTuple[G_KEY] == "player2Choose"), None)
        choosePlayerSheet = next((listIndex for listIndex, listTuple in enumerate(graphicDB) if listTuple[G_KEY] == "choosePlayerSheet"), None)
        arrow = next((listIndex for listIndex, listTuple in enumerate(graphicDB) if listTuple[G_KEY] == "arrow"), None)
        chooseArmy = next((listIndex for listIndex, listTuple in enumerate(audioDB) if listTuple[A_KEY] == "chooseArmy"), None)
 
        # size of a player's avatars
        spriteHeight = 64
        spriteWidth = 64

        # player avatar location on the screen
        avatarX = avatarY = 0
        
        # coordinates for 1P choose sprite animation
        playerX = 215
        playerY = 51

        # base coordinates for 1P choose player image
        animatePlayerX = 128
        animatePlayerY = 135

        # default arrow position for 1P
        arrowX = 150
        arrowY = 209
        
        # horizontally center our images on the screen on state load
        offsetX = 150

        # for drawing 2P graphics to the screen, move everything south
        offsetY = 215

        graphicDB[choosePlayerSheet][RECTANGLE].height = spriteHeight
        
        graphicDB[player1Choose][RECTANGLE].left = 211
        graphicDB[player2Choose][RECTANGLE].left = 0
        
        graphicDB[player1Choose][RECTANGLE].width /= 2
        graphicDB[player2Choose][RECTANGLE].width /= 2

        self.players[PLAYER1].avatar = (graphicDB[choosePlayerSheet][SURFACE], graphicDB[choosePlayerSheet][SURFACE].get_rect())
        self.players[PLAYER2].avatar = (graphicDB[choosePlayerSheet][SURFACE], graphicDB[choosePlayerSheet][SURFACE].get_rect())
        self.players[PLAYER1].avatar[RECTANGLE].width, self.players[PLAYER1].avatar[RECTANGLE].height = spriteWidth, spriteHeight
        self.players[PLAYER2].avatar[RECTANGLE].width, self.players[PLAYER2].avatar[RECTANGLE].height = spriteWidth, spriteHeight

        # list of rectangles to update on our screen
        dirtyRects = []
        
        # used to toggle the top coordinate of our choose player sheet animation
        nextAnimation = 65
        
        self.screen.blit(self.background, (0, 0))
        self.screen.blit(graphicDB[greyBorderBG][SURFACE], graphicDB[greyBorderBG][RECTANGLE])
        self.screen.blit(graphicDB[player1Choose][SURFACE], (playerX, playerY), graphicDB[player1Choose][RECTANGLE])
        self.screen.blit(graphicDB[player2Choose][SURFACE], (playerX, playerY + offsetY), graphicDB[player2Choose][RECTANGLE])
        self.screen.blit(graphicDB[choosePlayerSheet][SURFACE], (animatePlayerX, animatePlayerY), graphicDB[choosePlayerSheet][RECTANGLE])                                                    
        
        self.screen.blit(graphicDB[arrow][SURFACE], (arrowX, arrowY), graphicDB[arrow][RECTANGLE])

        pygame.display.update()
        
        pygame.time.set_timer(ANIMATE, 275)

        while (not self.finished):
            self.clock.tick(self.FPS)

            dirtyRects = []
                
            for event in pygame.event.get():
                if (event.type == QUIT):
                    pygame.quit()
                    sys.exit()

                elif (event.type == KEYDOWN):
                    # player 1 is selecting avatar
                    if (player1 is not done):
                        if (event.key == K_RETURN):
                            # avatar selected, set the viewing window for it
                            self.players[PLAYER1].avatar[RECTANGLE].left = arrowX - offsetX

                            # center the avatar image in place of the character selection sheet 
                            avatarX = animatePlayerX + (spriteWidth * 2.5)
                            avatarY = animatePlayerY

                            player1 = done

                            # erase the 1P arrow and character selection sheet 
                            dirtyRects.append( (arrowX, arrowY, graphicDB[arrow][RECTANGLE].width, graphicDB[arrow][RECTANGLE].height) )
                            dirtyRects.append( (animatePlayerX, animatePlayerY, graphicDB[choosePlayerSheet][RECTANGLE].width, graphicDB[choosePlayerSheet][RECTANGLE].height) )

                            # change to 2P choose player image
                            animatePlayerY += offsetY
                                                        
                            # default arrow position for 2P

                            arrowX = 150
                            arrowY += offsetY
                            
                            if ( (arrowX - offsetX) == self.players[PLAYER1].avatar[RECTANGLE].left ):
                                arrowX += spriteWidth
                            

                            graphicDB[player1Choose][RECTANGLE].left, graphicDB[player2Choose][RECTANGLE].left = graphicDB[player2Choose][RECTANGLE].left, graphicDB[player1Choose][RECTANGLE].left

                            dirtyRects.append( (arrowX, arrowY, graphicDB[arrow][RECTANGLE].width, graphicDB[arrow][RECTANGLE].height) )
                            dirtyRects.append( (playerX, playerY, graphicDB[player1Choose][RECTANGLE].width, graphicDB[player1Choose][RECTANGLE].height) )
                            dirtyRects.append( (playerX, playerY + offsetY, graphicDB[player2Choose][RECTANGLE].width, graphicDB[player2Choose][RECTANGLE].height) )

                            audioDB[chooseArmy][SOUND].play()

                        elif (event.key == K_LEFT):
                            dirtyRects.append( (arrowX, arrowY, graphicDB[arrow][RECTANGLE].width, graphicDB[arrow][RECTANGLE].height) )

                            arrowX -= spriteWidth

                            if (arrowX < offsetX):
                                arrowX = offsetX + (spriteWidth * 5)
                                
                            dirtyRects.append( (arrowX, arrowY, graphicDB[arrow][RECTANGLE].width, graphicDB[arrow][RECTANGLE].height) )
                        elif (event.key == K_RIGHT):
                            dirtyRects.append( (arrowX, arrowY, graphicDB[arrow][RECTANGLE].width, graphicDB[arrow][RECTANGLE].height) )
                            
                            arrowX += spriteWidth

                            # if our arrow goes more than 5 sprites to the east,
                            # we know we have passed the last character, thus
                            # set the arrow to its default position
                            if ( arrowX > offsetX + (spriteWidth * 5) ):
                                arrowX = offsetX
                                
                            dirtyRects.append( (arrowX, arrowY, graphicDB[arrow][RECTANGLE].width, graphicDB[arrow][RECTANGLE].height) )
                    # if we reach this point, player 2 is selecting avatar
                    elif (player2 is not done):
                        if (event.key == K_SPACE):
                            # avatar selected, set the viewing window for it
                            self.players[PLAYER2].avatar[RECTANGLE].left = arrowX - offsetX

                            player2 = done
                            
                            # erase the 2P player select, arrow, and character selection sheet images 
                            dirtyRects.append( (arrowX, arrowY, graphicDB[arrow][RECTANGLE].width, graphicDB[arrow][RECTANGLE].height) )
                            dirtyRects.append( (animatePlayerX, animatePlayerY, graphicDB[choosePlayerSheet][RECTANGLE].width, graphicDB[choosePlayerSheet][RECTANGLE].height) )
                            dirtyRects.append( (playerX, playerY + offsetY, graphicDB[player2Choose][RECTANGLE].width, graphicDB[player2Choose][RECTANGLE].height) )

                            animatePlayerX = animatePlayerY = arrowX = arrowY = -1000
                            graphicDB[player2Choose][RECTANGLE].left = 0
        
                            self.gameState.append("How To Play")
                            
                            self.finished = True
                            
                            length = int( ceil(audioDB[chooseArmy][SOUND].get_length()) ) * 1000
                            
                            audioDB[chooseArmy][SOUND].play()
                        elif (event.key == K_a):
                            dirtyRects.append( (arrowX, arrowY, graphicDB[arrow][RECTANGLE].width, graphicDB[arrow][RECTANGLE].height) )

                            arrowX -= spriteWidth

                            if ( (arrowX - offsetX) == self.players[PLAYER1].avatar[RECTANGLE].left ):
                                arrowX -= spriteWidth

                            if (arrowX < offsetX):
                                arrowX = offsetX + (spriteWidth * 5)

                            dirtyRects.append( (arrowX, arrowY, graphicDB[arrow][RECTANGLE].width, graphicDB[arrow][RECTANGLE].height) )
                        elif (event.key == K_d):
                            # erase previous arrow location
                            dirtyRects.append( (arrowX, arrowY, graphicDB[arrow][RECTANGLE].width, graphicDB[arrow][RECTANGLE].height) )

                            arrowX += spriteWidth

                            # if our arrow goes more than 5 sprites to the east,
                            # we know we have passed the last character, thus
                            # set the arrow to its initial position
                            if ( arrowX > offsetX + (spriteWidth * 5) ):
                                arrowX = offsetX
                                
                            if ( (arrowX - offsetX) == self.players[PLAYER1].avatar[RECTANGLE].left ):
                                arrowX += spriteWidth

                            # update current arrow location
                            dirtyRects.append( (arrowX, arrowY, graphicDB[arrow][RECTANGLE].width, graphicDB[arrow][RECTANGLE].height) )

                # create the animation effect
                elif (event.type == ANIMATE):
                    graphicDB[choosePlayerSheet][RECTANGLE].top, nextAnimation = nextAnimation, graphicDB[choosePlayerSheet][RECTANGLE].top
                    dirtyRects.append( (animatePlayerX, animatePlayerY, graphicDB[choosePlayerSheet][RECTANGLE].width, graphicDB[choosePlayerSheet][RECTANGLE].height) )
            
            ### PlayerSelectionDraw Method
            self.screen.blit(self.background, (0, 0))
            self.screen.blit(graphicDB[greyBorderBG][SURFACE], graphicDB[greyBorderBG][RECTANGLE])
            self.screen.blit(graphicDB[player1Choose][SURFACE], (playerX, playerY), graphicDB[player1Choose][RECTANGLE])
            self.screen.blit(graphicDB[player2Choose][SURFACE], (playerX, playerY + offsetY), graphicDB[player2Choose][RECTANGLE])
            self.screen.blit(graphicDB[choosePlayerSheet][SURFACE], (animatePlayerX, animatePlayerY), graphicDB[choosePlayerSheet][RECTANGLE])                                                    
            self.screen.blit(graphicDB[arrow][SURFACE], (arrowX, arrowY), graphicDB[arrow][RECTANGLE])

            # draw each player avatar to the screen once selected
            if (player1 is done):
                self.screen.blit(self.players[PLAYER1].avatar[SURFACE], (avatarX, avatarY), self.players[PLAYER1].avatar[RECTANGLE])

            if (player2 is done):
                self.screen.blit(self.players[PLAYER2].avatar[SURFACE], (avatarX, avatarY + offsetY), self.players[PLAYER2].avatar[RECTANGLE])

            ### PlayerSelectionUpdate Method
            # restart the title screen music when it stops
            if (pygame.mixer.music.get_busy() == False):
                pygame.mixer.music.play()
                
            pygame.display.set_caption("Checkers: on the battlefield (%d FPS)" % self.clock.get_fps())
            
            # only update necessary portions of the screen
            pygame.display.update(dirtyRects)

        pygame.time.wait(length + 2000)


    # === HowToPlay ===========================================================
    # This method handles the how to play game state. Simply put, this state
    # displays the rules of the game to the screen, until and key is pressed
    # from the keyboard. 
    #
    # input:
    #        nothing
    #
    # output
    #        nothing
    # =========================================================================

    def HowToPlay(self):
        select = next((listIndex for listIndex, listTuple in enumerate(audioDB) if listTuple[A_KEY] == "select"), None)
        howToPlay = next((listIndex for listIndex, listTuple in enumerate(graphicDB) if listTuple[G_KEY] == "howToPlay"), None)
        self.finished = not self.finished

        self.screen.blit(self.background, (0, 0))
        self.screen.blit(graphicDB[howToPlay][SURFACE], (0, 0))
        
        pygame.display.update() 

        while (not self.finished):
            self.clock.tick(self.FPS)
                
            for event in pygame.event.get():
                if (event.type == QUIT):
                    pygame.quit()
                    sys.exit()

                elif (event.type == KEYDOWN):
                    self.gameState.append("Play Game")
                    audioDB[select][SOUND].play()
                    self.finished = True

            self.screen.blit(graphicDB[howToPlay][SURFACE], (0, 0))

            if (pygame.mixer.music.get_busy() == False):
                pygame.mixer.music.play()
                
            pygame.display.set_caption("Checkers: on the battlefield (%d FPS)" % self.clock.get_fps())


    # === PlayGame ============================================================
    # This method handles the play game state. This is the method where the
    # actual game is played between two players
    # 
    # input:
    #        nothing
    #
    # output
    #        The game is over when a player loses all his/her tokens. A winner
    #        is then displayed to the screen. if no winner found after 12 total
    #        turns of each player having one token left, the game is a draw.
    # =========================================================================

    def PlayGame(self):
        # grab the indices of the sound effects used
        winner = next((listIndex for listIndex, listTuple in enumerate(graphicDB) if listTuple[G_KEY] == "winner"), None)

        # player1, player2, or draw on single image.
        graphicDB[winner][RECTANGLE].height /= 3

        # grab the indices of the sound effects used
        noMove = next((listIndex for listIndex, listTuple in enumerate(audioDB) if listTuple[A_KEY] == "noMove"), None)
        kill = next((listIndex for listIndex, listTuple in enumerate(audioDB) if listTuple[A_KEY] == "kill"), None)
        kingMe = next((listIndex for listIndex, listTuple in enumerate(audioDB) if listTuple[A_KEY] == "kingMe"), None)
        youWin = next((listIndex for listIndex, listTuple in enumerate(audioDB) if listTuple[A_KEY] == "youWin"), None)

        # used to index into position tuples
        X = 0
        Y = 1

        self.finished = not self.finished
 
        pygame.mixer.music.load("../audio/music/battle.ogg")

        # this will give us a list of playable squares that we can map to the board
        self.board.Initialize()

        # first 12 playable squares should be occupied by 1P
        self.players[PLAYER1].tokenPositions = list(self.board.squarePositions[0:12])
 
        # last 12 playable squares should be occupied by 2P
        self.players[PLAYER2].tokenPositions = list(self.board.squarePositions[20:33])

        # get the remaining playable squares not occupied by any player's token
        unoccupiedSquares = list( set(self.board.squarePositions) - (set(self.players[PLAYER1].tokenPositions) | set(self.players[PLAYER2].tokenPositions)) )
        unoccupiedSquares.sort()

        player1Tokens = []
        player2Tokens = []

        player1Group = pygame.sprite.RenderUpdates()
        player2Group = pygame.sprite.RenderUpdates()

        for token in xrange(0, 12):
            player1Tokens.append( CToken(self.players[PLAYER1].avatar[SURFACE], self.players[PLAYER1].avatar[RECTANGLE], self.players[PLAYER1].tokenPositions[token][X], self.players[PLAYER1].tokenPositions[token][Y], (self.board.squareWidth, self.board.squareHeight)) )
            player2Tokens.append( CToken(self.players[PLAYER2].avatar[SURFACE], self.players[PLAYER2].avatar[RECTANGLE], self.players[PLAYER2].tokenPositions[token][X], self.players[PLAYER2].tokenPositions[token][Y], (self.board.squareWidth, self.board.squareHeight)) )

        player1Tokens.sort(key = lambda token: (token.rect.x, token.rect.y), reverse = False)
        player2Tokens.sort(key = lambda token: (token.rect.x, token.rect.y), reverse = False)
        
        player1Group.add(player1Tokens)
        player2Group.add(player2Tokens)

        # tokens added to this group will also remain in their original group
        player1KingGroup = pygame.sprite.RenderUpdates()
        player2KingGroup = pygame.sprite.RenderUpdates()
        
        # this will tell us when to play the sound effect for getting a king
        kingTotal = 0

        # reset indices of player token list and the unoccupied squares list
        nextToken = nextUnoccupiedSquare = 0

        # always have 1P as the starting player 
        currentPlayer = player1Tokens

        # this is the position to highlight 1P first token on the screen 
        outlineX, outlineY = currentPlayer[0].rect.left, currentPlayer[0].rect.top

        # used to distinguish whose turn it is within our nested loop during
        # potential character movement
        restartTurn = False
        done = True
        player1 = player2 = not done

        # if this changes, we have a winner
        winStatus = -1

        # center checker board within the screen
        centerX = ( (self.screenWidth - (self.board.squareWidth * 8) ) / 2 )
        centerY = ( (self.screenHeight - (self.board.squareHeight * 8) ) / 2 )

        # need to draw all tokens to a surface so we can center the game
        tokens = self.board.board.copy()
        tokens.fill( pygame.color.Color("#FFFFFF") )
        tokens.set_colorkey(self.players[PLAYER2].avatar[SURFACE].get_at( (0, 0) ), RLEACCEL)

        # small surface to use for highlighting squares
        outlineSquare = pygame.Surface( (self.board.squareWidth, self.board.squareHeight) )
        outlineSquare.fill(self.board.boardColor)
        outlineSquare.set_colorkey(outlineSquare.get_at( (0, 0) ), RLEACCEL)
        outlineRect = outlineSquare.get_rect()
        
        # holds the movement sequence during a player's turn
        movements = []
        
        # holds all rects that need to be updated on the screen
        dirtyRects = []
        
        # updates dirty rects with offset values (if needed)
        updatedDirtyRects = []

        pygame.mixer.music.play()
        
        self.screen.blit(self.background, (0, 0))
        self.screen.blit(self.board.board, (centerX, centerY))
        player1Group.draw(tokens)
        player2Group.draw(tokens)
        self.screen.blit(tokens, (centerX, centerY))
        # outline square of 1P first token
        pygame.draw.rect(outlineSquare, pygame.color.Color("#00FF00"), currentPlayer[0].rect.inflate(-2, -2), 3)
        self.screen.blit(outlineSquare, (outlineX + centerX, outlineY + centerY))

        pygame.display.update()  
        
        while (not self.finished):
            self.clock.tick(self.FPS)

            for event in pygame.event.get():
                if (event.type == QUIT):
                    pygame.quit()
                    sys.exit()
                elif (event.type == pygame.KEYDOWN):
                    if ( (player1 is not done)  &  (currentPlayer == player1Tokens) ):
                        if (event.key == K_RETURN): 
                            unoccupiedSquareX, unoccupiedSquareY = unoccupiedSquares[nextUnoccupiedSquare]
                            
                            while (player1 is not done):
                                for event in pygame.event.get():
                                    if (event.type == QUIT):
                                        pygame.quit()
                                        sys.exit()      
                                    elif (event.type == KEYDOWN):
                                        # signals the player is finished with their turn
                                        if (event.key == K_1):
                                            # if true then update player token locations
                                            if (self.ValidateMove("1", player1Tokens[nextToken], movements, player2Tokens, player2Group, kill) == True):
                                                # check if current player should be awarded king status
                                                # 128 is my current location of kings on the Y plane
                                                # of the sprite sheet used for tokens
                                                kingTotal = len(player1KingGroup)

                                                player1Group.update(self.board.squareHeight * 7, 128, player1KingGroup)
                                                
                                                if (len(player1KingGroup) > kingTotal):
                                                    audioDB[kingMe][SOUND].play()

                                                player1Group.clear(tokens, self.board.board)
                                                player2Group.clear(tokens, self.board.board)
                                                player1Group.draw(tokens)
                                                player2Group.draw(tokens)
                                                currentPlayer = player2Tokens
                                                restartTurn = False
                                                
                                                if (len(player1Tokens) == len(player2Tokens) == 1):
                                                    self.drawCounter += 1
                                                
                                                
                                                del self.players[PLAYER1].tokenPositions[:]
                                                del self.players[PLAYER2].tokenPositions[:]
                                                
                                                for p1Position in xrange(len(player1Tokens)):
                                                    self.players[PLAYER1].tokenPositions.append( (player1Tokens[p1Position].rect.left, player1Tokens[p1Position].rect.top) )
                                                
                                                for p2Position in xrange(len(player2Tokens)):
                                                    self.players[PLAYER2].tokenPositions.append( (player2Tokens[p2Position].rect.left, player2Tokens[p2Position].rect.top) )

                                                unoccupiedSquares = list( set(self.board.squarePositions) - (set(self.players[PLAYER1].tokenPositions) | set(self.players[PLAYER2].tokenPositions)) )
                                                unoccupiedSquares.sort()
                                            else:
                                                restartTurn = True
                                                audioDB[noMove][SOUND].play()
                                                
                                            for nextRect in xrange(len(movements)):
                                                dirtyRects.append(pygame.Rect(movements[nextRect][X], movements[nextRect][Y], self.board.squareWidth, self.board.squareHeight))

                                            dirtyRects.append(pygame.Rect(unoccupiedSquares[nextUnoccupiedSquare][X],unoccupiedSquares[nextUnoccupiedSquare][Y], self.board.squareWidth, self.board.squareHeight))
                                            del movements [:]
                                            nextToken = nextUnoccupiedSquare = 0
                                            player1 = done
                                            player2 = not done
                                        # add to the movements of selected token
                                        elif (event.key == K_RETURN):
                                            # toggles the next movement to a movement list 
                                            self.AddToPath(unoccupiedSquares[nextUnoccupiedSquare], movements)
                                        elif (event.key == K_LEFT):
                                            nextUnoccupiedSquare -= 1
                                            
                                            if (nextUnoccupiedSquare < 0):
                                                nextUnoccupiedSquare = len(unoccupiedSquares) - 1
                                                
                                            unoccupiedSquareX, unoccupiedSquareY = unoccupiedSquares[nextUnoccupiedSquare]
                                        elif (event.key == K_RIGHT):
                                            nextUnoccupiedSquare += 1
                        
                                            if (nextUnoccupiedSquare == len(unoccupiedSquares)):
                                                nextUnoccupiedSquare = 0

                                            unoccupiedSquareX, unoccupiedSquareY = unoccupiedSquares[nextUnoccupiedSquare]
                                            
                                if ( (player1 is not done)  |  (restartTurn == False) ):
                                    # draw surfaces to the screen to maintain screen state
                                    self.screen.blit(self.background, (0, 0))
                                    self.screen.blit(self.board.board, (centerX, centerY))
                                    self.screen.blit(tokens, (centerX, centerY))
                                    self.screen.blit(outlineSquare, (outlineX + centerX, outlineY + centerY))
                                    self.screen.blit(outlineSquare, (unoccupiedSquareX + centerX, unoccupiedSquareY + centerY))
    
                                    # draw each selected movements square to the screen
                                    for eachSquare in xrange(len(movements)):
                                        self.screen.blit(outlineSquare, (movements[eachSquare][X] + centerX, movements[eachSquare][Y] + centerY))
                                
                                # need to update music, display, and frame rate
                                # since we're in a nested loop
                                if (pygame.mixer.music.get_busy() == False):
                                    pygame.mixer.music.play()
                        
                                pygame.display.set_caption("Checkers: on the battlefield (%d FPS)" % self.clock.get_fps())
                                pygame.display.update()         
                        elif (event.key == K_LEFT):
                            nextToken -= 1
                            
                            if (nextToken < 0):
                                nextToken = len(player1Tokens) - 1
                        elif (event.key == K_RIGHT):
                            nextToken += 1
        
                            if (nextToken == len(player1Tokens)):
                                nextToken = 0

                        if (restartTurn == True):
                            restartTurn = False
                            currentPlayer = player1Tokens
                            player1 = not done
                            player2 = done                 
                    elif ( (player2 is not done)  &  (currentPlayer == player2Tokens) ):
                        if (event.key == K_SPACE): 
                            unoccupiedSquareX, unoccupiedSquareY = unoccupiedSquares[nextUnoccupiedSquare]
                            
                            while (player2 is not done):
                                for event in pygame.event.get():
                                    if (event.type == QUIT):
                                        pygame.quit()
                                        sys.exit()      
                                    elif (event.type == KEYDOWN):
                                        # signals the player is finished with their turn
                                        if (event.key == K_2):
                                            # if true then update player token locations
                                            if (self.ValidateMove("2", player2Tokens[nextToken], movements, player1Tokens, player1Group, kill) == True):
                                                # check if current player should be awarded king status
                                                # 128 is my current location of kings on the Y plane
                                                # of the sprite sheet used for tokens
                                                kingTotal = len(player2KingGroup)
                                          
                                                # 128 is the magic number for the y-coordinate of
                                                # king sprites on the spritesheet used in game      
                                                player2Group.update(0, 128, player2KingGroup)

                                                if (len(player2KingGroup) > kingTotal):
                                                    audioDB[kingMe][SOUND].play()

                                                player1Group.clear(tokens, self.board.board)
                                                player2Group.clear(tokens, self.board.board)
                                                player1Group.draw(tokens)
                                                player2Group.draw(tokens)
                                                currentPlayer = player1Tokens
                                                restartTurn = False
                                                
                                                if (len(player1Tokens) == len(player2Tokens) == 1):
                                                    self.drawCounter += 1
                                                
                                                del self.players[PLAYER1].tokenPositions[:]
                                                del self.players[PLAYER2].tokenPositions[:]
                                                
                                                for p1Position in xrange(len(player1Tokens)):
                                                    self.players[PLAYER1].tokenPositions.append( (player1Tokens[p1Position].rect.left, player1Tokens[p1Position].rect.top) )
                                                
                                                for p2Position in xrange(len(player2Tokens)):
                                                    self.players[PLAYER2].tokenPositions.append( (player2Tokens[p2Position].rect.left, player2Tokens[p2Position].rect.top) )

                                                unoccupiedSquares = list( set(self.board.squarePositions) - (set(self.players[PLAYER1].tokenPositions) | set(self.players[PLAYER2].tokenPositions)) )
                                                unoccupiedSquares.sort()
                                            else:
                                                restartTurn = True
                                                audioDB[noMove][SOUND].play()
                                                    
                                            for nextRect in xrange(len(movements)):
                                                dirtyRects.append(pygame.Rect(movements[nextRect][X], movements[nextRect][Y], self.board.squareWidth, self.board.squareHeight))

                                            dirtyRects.append(pygame.Rect(unoccupiedSquares[nextUnoccupiedSquare][X],unoccupiedSquares[nextUnoccupiedSquare][Y], self.board.squareWidth, self.board.squareHeight))
                                            del movements [:]
                                            nextToken = nextUnoccupiedSquare = 0
                                            player1 = not done
                                            player2 = done
                                        # add to the movements of selected token
                                        elif (event.key == K_SPACE):
                                            # toggles the next movement to a movement list 
                                            self.AddToPath(unoccupiedSquares[nextUnoccupiedSquare], movements)
                                        elif (event.key == K_a):
                                            nextUnoccupiedSquare -= 1
                                            
                                            if (nextUnoccupiedSquare < 0):
                                                nextUnoccupiedSquare = len(unoccupiedSquares) - 1
                                                
                                            unoccupiedSquareX, unoccupiedSquareY = unoccupiedSquares[nextUnoccupiedSquare]
                                        elif (event.key == K_d):
                                            nextUnoccupiedSquare += 1
                        
                                            if (nextUnoccupiedSquare == len(unoccupiedSquares)):
                                                nextUnoccupiedSquare = 0

                                            unoccupiedSquareX, unoccupiedSquareY = unoccupiedSquares[nextUnoccupiedSquare]
                                            
                                if ( (player2 is not done)  |  (restartTurn == False) ):
                                    # draw surfaces to the screen to maintain screen state
                                    self.screen.blit(self.background, (0, 0))
                                    self.screen.blit(self.board.board, (centerX, centerY))
                                    self.screen.blit(tokens, (centerX, centerY))
                                    self.screen.blit(outlineSquare, (outlineX + centerX, outlineY + centerY))
                                    self.screen.blit(outlineSquare, (unoccupiedSquareX + centerX, unoccupiedSquareY + centerY))
    
                                    # draw each selected movements square to the screen
                                    for eachSquare in xrange(len(movements)):
                                        self.screen.blit(outlineSquare, (movements[eachSquare][X] + centerX, movements[eachSquare][Y] + centerY))

                                # need to update music, display, and frame rate
                                # since we're in a nested loop
                                if (pygame.mixer.music.get_busy() == False):
                                    pygame.mixer.music.play()
                        
                                pygame.display.set_caption("Checkers: on the battlefield (%d FPS)" % self.clock.get_fps())
                                pygame.display.update()         
                        elif (event.key == K_a):
                            nextToken -= 1
                            
                            if (nextToken < 0):
                                nextToken = len(player2Tokens) - 1
                        elif (event.key == K_d):
                            nextToken += 1
        
                            if (nextToken == len(player2Tokens)):
                                nextToken = 0

                        if (restartTurn == True):
                            restartTurn = False
                            currentPlayer = player2Tokens
                            player1 = done
                            player2 = not done                
                    
            # we need to add the previous and next outlined square
            # positions to the list of updated screen locations
            outlineRect.x, outlineRect.y = outlineX, outlineY
            dirtyRects.append(outlineRect.copy())

            # this block safeguards against indexing the 0th element of our
            # current player's token list.
            if (len(currentPlayer) != 0):
                outlineX, outlineY = currentPlayer[nextToken].rect.x, currentPlayer[nextToken].rect.y

            outlineRect.x, outlineRect.y = outlineX, outlineY
            dirtyRects.append(outlineRect.copy())

            # since our sprites and board are offset from the left to be
            # centered within the window, we must update the dirty rects
            # with the appropriate offset values
            for i in xrange(len(dirtyRects)):
                updatedDirtyRects.append(dirtyRects[i].copy())
                updatedDirtyRects[i].x += centerX
                updatedDirtyRects[i].y += centerY

            ### PlayGameDraw
            self.screen.blit(self.background, (0, 0))
            self.screen.blit(self.board.board, (centerX, centerY))           
            self.screen.blit(tokens, (centerX, centerY))
            self.screen.blit(outlineSquare, (outlineX + centerX, outlineY + centerY)) 
            
            ### PlayGameUpdate
            # restart the background music when it stops
            if (pygame.mixer.music.get_busy() == False):
                pygame.mixer.music.play()

            winStatus = self.IsAWinner(player1Tokens, player2Tokens)

            # do we have a winner?
            if (winStatus != -1 ):
                if (winStatus == 0):
                    # draw
                    graphicDB[winner][RECTANGLE].y = 240
                elif (winStatus == 1):
                    # player 1
                    graphicDB[winner][RECTANGLE].y = 0
                elif (winStatus == 2):
                    # player 2
                    graphicDB[winner][RECTANGLE].y = 120
                    
                self.screen.blit(graphicDB[winner][SURFACE], (centerX, (self.screenHeight - graphicDB[winner][RECTANGLE].height) / 2), graphicDB[winner][RECTANGLE])

                # since we have a winner, just update the portion of the screen
                # that displays the winner
                updatedDirtyRects = [pygame.Rect(centerX, (self.screenHeight - graphicDB[winner][RECTANGLE].height) / 2, graphicDB[winner][RECTANGLE].width, graphicDB[winner][RECTANGLE].height)]

                audioDB[youWin][SOUND].play()

                self.finished = True

            pygame.display.set_caption("Checkers: on the battlefield (%d FPS)" % self.clock.get_fps())
            pygame.display.update(updatedDirtyRects)
            
            del dirtyRects [:]
            del updatedDirtyRects [:]
            
            # let the winner feel good for 7 seconds before exiting
            if (winStatus != -1 ):
                pygame.time.wait(7000)



    # =========================================================================
    # Helper Methods
    # =========================================================================



    # === AddToPath ===========================================================
    # This method toggles a particular (x, y) coordinate to a list of potential
    # token movement.
    # 
    # input:
    #        selectedSquare  --  (x, y) coordinate as a tuple to toggle on/off
    #                            within a token's movement list
    #        movements       --  current list of (x, y) tuples that represent
    #                            a particular token's potential movements
    #
    # output
    #        Returns true if the tuple was added to the list. Otherwise, return
    #        false because the tuple is being removed from the list.
    # =========================================================================

    def AddToPath(self, selectedSquare, movements):
        duplicate = next((index for index, square in enumerate(movements) if  square == selectedSquare), None)

        added = True

        if (duplicate == None):
            movements.append(selectedSquare)
            return added
        else:
            del movements[duplicate]
            return not added


    # === ValidateMove ========================================================
    # This method will take the movement list of a token and validate or
    # invalidate the list, depending upon if the moves adhere to the rules of
    # the game's movement or not.
    # 
    # input:
    #        whichPlayer     --  which player is trying to move
    #        token           --  which token is trying to move
    #        movements       --  sequential list of (x, y) tuples that map to
    #                            the desired squares of a token'a movement 
    #        opposingTokens  --  list of all the enemy sprites
    #        opposingGroup   --  group object of all enemy sprites
    #        kill            --  index to the kill sound effect when an enemy
    #                            sprite is destroyed
    #
    # output
    #        Returns true if the entire movement list of the token is legal.
    #        Otherwise, return false because a move was found to be illegal.
    #        Also, this method takes care of collision testing and destroys
    #        the necessary tokens. we will refactor this method in the future.
    # =========================================================================

    def ValidateMove(self, whichPlayer, token, movements, opposingTokens, opposingGroup, kill):
        X = 0
        Y = 1

        validMove = False
        multiplier = None
        hasMoved = False
        hasJumped = False
        squareWidth = self.board.squareWidth
        squareHeight = self.board.squareHeight
        
        # if opposing tokens die, we will save the original opposing tokens
        # list to this temp list. next, we will delete all opposing tokens
        # within the original opposing tokens list. finally, we append only the
        # opposing tokens who are still alive from this temp list to the
        # original opposing tokens list. this enables to us to keep the altered
        # state of our opposing tokens list after leaving this function.
        tempOpposingTokens = []
        
        # collision testing
        opposingCollide = []
        tempOpposingCollide = []

        # save the original position of the token
        originalX, originalY = token.rect.x, token.rect.y

        # used to determine if the king if moving north of south
        deltaY = 0
        
        # if we don't have any movement, just leave
        if (movements == []):
            return validMove

        if (whichPlayer == "2"):
            squareHeight = -squareHeight

        # if we have one movement, it must be a single neighboring diagonal space or a jump
        for nextSquare in xrange(len(movements)):
            # this block says we moved one space and have additional moves to
            # make, or we had an incorrect jump. Thus, make our overall movement invalid
            if ( (nextSquare > 0)  &  ( (hasMoved == True)  |   (hasJumped == False)  ) ):
                token.rect.x, token.rect.y = originalX, originalY
                validMove = False
                return validMove
 
            # check if a king token
            if (len(token.groups()) == 2):
                
                deltaY = movements[nextSquare][Y] - token.rect.y
                
                if ( deltaY > 0 ):
                    squareHeight = abs(squareHeight)
                else:
                    squareHeight = abs(squareHeight) * (-1)

            # move one square
            if ( (movements[nextSquare][Y] == token.rect.y + squareHeight) ):
                if ( (hasMoved == True)  |  (hasJumped == True) ):
                    validMove = False
                    break

                multiplier = 1

            # move two squares (jump)
            elif (movements[nextSquare][Y] == token.rect.y + (squareHeight * 2)):
                if (hasMoved == True):
                    validMove = False
                    break

                multiplier = 2

            if ( (multiplier == 1)  |  (multiplier == 2) ):
                # was this move valid?
                if ( (movements[nextSquare][X] == token.rect.x - (squareWidth * multiplier))  |  (movements[nextSquare][X] == token.rect.x + (squareWidth * multiplier)) ):
                # check for jump
                    if (multiplier == 2):
                        # test to see if we're jumping over an opposing token
                        token.rect.x, token.rect.y = (token.rect.x + movements[nextSquare][X]) / 2, token.rect.y + squareHeight
                        tempOpposingCollide = pygame.sprite.spritecollide(token, opposingGroup, False)

                        # if we are then add it to the list of opposing tokens to kill
                        if (tempOpposingCollide != []):
                            opposingCollide += tempOpposingCollide
                            multiplier = None
                            hasJumped = True
                        else:
                            validMove = False
                            break
                    else:
                        hasMoved = True
                        
                    # advance the token to its next movement position
                    token.rect.x = movements[nextSquare][X]
                    token.rect.y = movements[nextSquare][Y]
                    
                    validMove = True
            
                else:
                    validMove = False
                    break
            else:
                validMove = False
                break
        
        if (validMove == False):
            token.rect.x, token.rect.y = originalX, originalY
        
        elif (opposingCollide != []):
            for token in xrange(len(opposingCollide)):
                opposingCollide[token].kill()
                    
            tempOpposingTokens = list(opposingTokens)
            
            # make amendments to our original opposing tokens list
            del opposingTokens [:]
            opposingTokens.extend( list( set(tempOpposingTokens) - set(opposingCollide) ) )
            audioDB[kill][SOUND].play()

        opposingTokens.sort(key = lambda token: (token.rect.x, token.rect.y), reverse = False)
        
        return validMove


    # === IsAWinner ===========================================================
    # This method simply determines a winner by checking the lengths of each
    # of the player's token list. If any lists are zero, the opposing player
    # is the winner. If both players have only one token left, we begin
    # starting a counter that will call the game a draw in 12 total turns.
    # 
    # input:
    #        player1  --  1P list of remaining tokens
    #        player2  --  2P list of remaining tokens
    #
    # output
    #        The following return values describe the win state:
    #            0 = Draw
    #            1 = 1P
    #            2 = 2P
    #            -1 = No winner (the game continues)
    # =========================================================================

    def IsAWinner(self, player1, player2):
        # count the number of remaining tokens each player has left in the game
        p1Count = len(player1)
        p2Count = len(player2)

        # each player gets six turns upon sudden death
        # before calling the game a draw
        if (self.drawCounter == 12):
            print "Draw!"
            return 0
        elif (p2Count == 0):
            print "Player 1 Wins!"
            return 1
        elif (p1Count == 0):
            print "Player 2 Wins!"
            return 2
        
        # no winner is present
        return -1
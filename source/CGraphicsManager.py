# =============================================================================
# File  : CGraphicsManager.py (Spring 2011)
# Author: Troy Stump (troystump@csu.fullerton.edu)
# CWID  : 894788678
# Class : 386 Professor Shafae
# Assign: 05
# =============================================================================
# This class handles loading graphic files into memory.
# =============================================================================

import os
import pygame
from pygame.constants import RLEACCEL


class CGraphicsManager:   
    # === __init__ ============================================================
    # This is the class initializer method.
    #
    #    input:
    #            directory  --  path to the graphics folder
    #
    #    output:
    #            nothing
    # =========================================================================

    def __init__(self, directory):
        self.directory = directory


    # === LoadImage ===========================================================
    # This method loads an image into memory.
    #
    #    input:
    #            fileName  --  name of the graphic file to be loaded
    #            colorkey  --  set color transparent (-1 gets color at (0,0))
    #
    #    output:
    #            If successful, returns a tuple of the loaded image and its
    #             respective rectangle. Otherwise, raises an error message.
    # =========================================================================

    def LoadImage(self, name, colorkey = None):
        fullname = os.path.join(self.directory, name)
        
        try:
            image = pygame.image.load(fullname)
        except pygame.error, message:
            print 'Cannot load image:', name
            raise SystemExit, message

        image = image.convert()

        if colorkey is not None:
            if colorkey is -1:
                colorkey = image.get_at( (0,0) )
        
            image.set_colorkey(colorkey, RLEACCEL)

        return image, image.get_rect()